.ONESHELL:
.SHELL := /usr/bin/bash
.PHONY: dev devd prod

help:
	@grep -E '^[a-zA-Z_-]+:.*?## .*$$' $(MAKEFILE_LIST) | sort | awk 'BEGIN {FS = ":.*?## "}; {printf "\033[35m%-10s\033[0m %s\n", $$1, $$2}'
pull:
	@alias p='git pull 2>&1 | tail -4'
	@p
dev:pull ## Apply the Terraform chainges on Dev Environment
	@cd dev
	@terraform init
	@terraform apply --auto-approve

devd:pull ## Apply the Terraform chainges on Dev Environment for Detroy
	@cd dev
	@terraform init #AAAAAAAAAAAAAA
	@terraform destroy --auto-approve

prod:pull ## Apply the Terraform chainges on Prod Envinorment
	@cd prod
	@terraform init
	@terraform apply --auto-approve

qa:pull ## Apply the Terraform chainges on Prod Envinorment
	@cd qa
	@terraform init
	@terraform apply --auto-approve
