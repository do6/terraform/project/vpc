output "VPC_ID" {
  value = module.vpc_peering_nat.VPC-ID
}
output "MANAGEMENT_VPC_ID" {
  value = var.MANAGEMENT_VPC_ID
}
output "PUBLIC_SUBNTS_IDS" {
  value = module.vpc_peering_nat.public_subnets
}
output "PRIVATE_SUBNETS_IDS" {
  value = module.vpc_peering_nat.private_subnets
}
