provider "aws" {
  region = "us-east-1"
}
terraform {
  backend "s3" {
    bucket = "sudheer919"
    key    = "project/vpc/dev/terraform.tfstate"
    region = "us-east-1"
//  dynamodb_table = "tlocking9"
  }
}
