module "vpc_peering_nat" {
  source                    = "../module"
  MANAGEMENT_VPC_ID         = var.MANAGEMENT_VPC_ID
  TAGS                      = var.TAGS
  VPC_CIDR                  = var.VPC_CIDR
}