VPC_CIDR          = "10.1.0.0/16"
TAGS              = {
  PROJECT         = "Robo-Shop"
  ENV             = "Dev"
  GIT_REPO        = "https://gitlab.com/do6/terraform/project/vpc.git"
  IAC             = "Terraform"
}
MANAGEMENT_VPC_ID = "vpc-06e15c78e03608f42"