resource "aws_internet_gateway" "robo_shop_igw" {
  vpc_id    = aws_vpc.rs_vpc.id

  tags      = {
    Name    = "Robo_Shop_igw"
  }
}