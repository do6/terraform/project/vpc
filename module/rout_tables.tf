# Adding Management CIDR to Robo_shop
# Hear attaching the management vpc-cidr to robo_shop
resource "aws_route_table" "peering_rout_management_cidr" {
  vpc_id                      = aws_vpc.rs_vpc.id

  route {
    cidr_block                = data.aws_vpc.management_vpc_id.cidr_block
    vpc_peering_connection_id = aws_vpc_peering_connection.peering_b_w_management_and_robo_shop.id
  }

#creating public rout to robo_shop_ngw
  route {
    cidr_block                = "0.0.0.0/0"
    nat_gateway_id            = aws_nat_gateway.robo_shop_ngw.id
  }

  tags                        = {
    Name                      = "peering_rout_management_cidr"
  }
}

# Adding Robo_shop CIDR to Management
# Hear attaching the robo shop cidr to management

resource "aws_route" "routing_to_Robo_shop_CIDR_to_Management" {
  route_table_id            = local.MANAGEMENT_RTS
  destination_cidr_block    = var.VPC_CIDR
  vpc_peering_connection_id = aws_vpc_peering_connection.peering_b_w_management_and_robo_shop.id

}
##################################################################################################################
#                         PUBLIC-ROUT_TABLES
##################################################################################################################
resource "aws_route_table" "public_rout_table" {
  vpc_id                    = aws_vpc.rs_vpc.id

  route {
    cidr_block              = "0.0.0.0/0"
    gateway_id              = aws_internet_gateway.robo_shop_igw.id
  }

  tags                      = {
    Name                    = "main"
  }
}