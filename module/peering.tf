resource "aws_vpc_peering_connection" "peering_b_w_management_and_robo_shop" {
  peer_owner_id     = data.aws_caller_identity.current.account_id
  peer_vpc_id       = var.MANAGEMENT_VPC_ID
  vpc_id            = aws_vpc.rs_vpc.id
  auto_accept       = true

  tags              = {
    Name            = "peering_b/w_management_&_robo_shop"
  }
}
