locals {
  vpc_name        =   "${var.TAGS["PROJECT"]}-${var.TAGS["ENV"]}-VPC"
  Env             =   var.TAGS["ENV"]
  Git             =   var.TAGS["GIT_REPO"]
  Iac             =   var.TAGS["IAC"]
  MANAGEMENT_RTS  =   join ("," , data.aws_route_tables.management_rts.ids)
}