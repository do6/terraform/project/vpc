data "aws_availability_zones" "available" {
  state = "available"
}

data "aws_caller_identity" "current" {}



data "aws_vpc" "management_vpc_id" {
  id = var.MANAGEMENT_VPC_ID
}

data "aws_route_tables" "management_rts" {
  vpc_id = var.MANAGEMENT_VPC_ID
}