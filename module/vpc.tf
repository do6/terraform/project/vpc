resource "aws_vpc" "rs_vpc" {
  cidr_block            = var.VPC_CIDR
  instance_tenancy      = "default"
  enable_dns_hostnames  = true

  tags                  = {
    Name                =   local.vpc_name
    Env                 =   local.Env
    Git_repo            =   local.Git
    Iac                 =   local.Iac
  }
}