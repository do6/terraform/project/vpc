##################################################################################################################
#                         PRIVATE-SUBNETS
##################################################################################################################
# private subnets

resource "aws_subnet" "private_subnets" {
  count                   = length(data.aws_availability_zones.available.names)
  vpc_id                  = aws_vpc.rs_vpc.id
  cidr_block              = cidrsubnet(var.VPC_CIDR,8 ,count.index )
  availability_zone       = element(data.aws_availability_zones.available.names,count.index )
  map_public_ip_on_launch = true


  tags                    = {
    Name                  = "private_subnets"
  }
}

# subnet Association for PRIVATE-SUBNETS
resource "aws_route_table_association" "private_rta" {
  count                   = length(data.aws_availability_zones.available.names)
  subnet_id               = element(aws_subnet.private_subnets.*.id,count.index )
  route_table_id          = aws_route_table.peering_rout_management_cidr.id
}

##################################################################################################################
#                         PUBLIC-SUBNETS
##################################################################################################################
# public subnets

resource "aws_subnet" "public_subnets" {
  count                   = length(data.aws_availability_zones.available.names)
  vpc_id                  = aws_vpc.rs_vpc.id
  cidr_block              = cidrsubnet(var.VPC_CIDR,8,count.index+length(data.aws_availability_zones.available.names))
  availability_zone       = element(data.aws_availability_zones.available.names,count.index )
  tags                    = {
    Name                  = "Public_subnets"
  }
}
# Creating Rout Table Association

resource "aws_route_table_association" "public_rta" {
  count                   = length(data.aws_availability_zones.available.names)
  subnet_id               = element(aws_subnet.public_subnets.*.id,count.index )
  route_table_id          = aws_route_table.public_rout_table.id
}
