#Creating Elastic ip
resource "aws_eip" "nat_eip" {}
#Creating Nat_Gateway

resource "aws_nat_gateway" "robo_shop_ngw" {

  allocation_id     = aws_eip.nat_eip.id
  subnet_id         = element(aws_subnet.public_subnets.*.id,1)

  tags              = {
    Name            = "ROBO_SHOP_NAT_GATE"
  }
}